#!/usr/bin/env bash

#Command Tracing
#set -x

# Benchmark info
echo "TIMING - Starting main script at: $(date)"

# Clean up the modules in working environment
module -q reset

# Prepare environment based on submit type
if [[ "$OOD_SUBMIT_TYPE" =~ ^sif.* ]]; then
	if [ ! -f "$OOD_SIF" ]; then
		echo "Error: User provided container ($OOD_SIF) does not exist! Stopping!!"
		exit 21
	fi

	SIF="$OOD_SIF"
else
	# Load Initial Modules
	# shellcheck disable=SC2086
	module load $OOD_MODULES

	# Report Modules
	echo ""
	echo "The Following Modules Are Loaded:"
	module --terse list
	echo ""

	# Get SIF Image from Module
	if [ -z "$SIF" ] && [ -f "$SIF" ]; then
		echo "Error: Module did not set 'SIF' environment variable for file is missing! Stopping!!"
		exit 22
	fi
fi
export SIF

# Report SIF Image
echo ""
echo "The Following SIF Image Is Selected:"
echo "$SIF"
echo ""

# CD Into Desired Working Directory
if [ -d "$OOD_WD" ]; then
	cd "$OOD_WD" || true
else
	cd "$HOME" || true
fi

# Launch Frontend
if [ "$OOD_INTERFACE" == 'code-server' ]; then
	# Setup dirs for code-server permanant data
	CODE_SERVER_DATAROOT="$HOME/.local/share/code-server"
	mkdir -p "$CODE_SERVER_DATAROOT/extensions"

	# Set Default Workspace File If Needed
	if [ "$OOD_WD" == "$HOME" ]; then
		OOD_WD="$OOD_SR/job.code-workspace"
	fi

	"$OOD_SR/bin/code-server" \
		--auth="password" \
		--bind-addr="0.0.0.0:${PORT}" \
		--disable-telemetry \
		--disable-update-check \
		--extensions-dir="$CODE_SERVER_DATAROOT/extensions" \
		--user-data-dir="$CODE_SERVER_DATAROOT" \
		--ignore-last-opened \
		"$OOD_WD"

elif [ "$OOD_INTERFACE" == 'jupyter-lab' ]; then
	cat <<-EOL > "$OOD_SR/config.py"
		c.NotebookApp.ip = '0.0.0.0'
		c.NotebookApp.port = ${PORT}
		c.NotebookApp.port_retries = 0
		c.NotebookApp.password = u'sha1:${SALT}:${PASSWORD_SHA1}'
		c.NotebookApp.base_url = '/node/${HOST}/${PORT}/'
		c.NotebookApp.open_browser = False
		c.NotebookApp.allow_origin = '*'
		c.NotebookApp.notebook_dir = '${OOD_WD}'
		c.NotebookApp.disable_check_xsrf = True
	EOL
	"$OOD_SR/bin/jupyter-lab" --config="$OOD_SR/config.py"

elif [ "$OOD_INTERFACE" == 'jupyter-notebook' ]; then
	cat <<-EOL > "$OOD_SR/config.py"
		c.NotebookApp.ip = '0.0.0.0'
		c.NotebookApp.port = ${PORT}
		c.NotebookApp.port_retries = 0
		c.NotebookApp.password = u'sha1:${SALT}:${PASSWORD_SHA1}'
		c.NotebookApp.base_url = '/node/${HOST}/${PORT}/'
		c.NotebookApp.open_browser = False
		c.NotebookApp.allow_origin = '*'
		c.NotebookApp.notebook_dir = '${OOD_WD}'
		c.NotebookApp.disable_check_xsrf = True
	EOL
	"$OOD_SR/bin/jupyter-notebook" --config="$OOD_SR/config.py"

elif [ "$OOD_INTERFACE" == 'pluto' ]; then
	"$OOD_SR/bin/pluto"

else
	echo "Error: '$OOD_INTERFACE' is not a valid option! Stopping!!"
	exit 35
fi